Assignment 2: Add User Management to a CRUD Application
=======================================================

In this assignment, you will make a version of your CRUD Web application that
functions as a rudimentary social bookmarking site: there can be multiple
users. Every user can read every user’s list of books, but only create, update
and delete his or her own books. PHP’s session management is used to
authenticate users.

Step 1: Get the Starter Code
----------------------------

On the command line, go into your `ddwt2016` directory, i.e. your local copy of
the repository that you created last week. Update it using the following
command.

    git pull

This adds a directory `week2/assignment/bookcrud2`, which contains the starter
code for this assignment.

Step 2: Modify the Database
---------------------------

Create a table called users with at least the following fields:

* `user_id` (an integer, primary key)
* `name` (the user name)
* `password_hash` (should be of type `CHAR(60)`)

Also add a `user_id` column to the `books` table to indicate which user each
book belongs to.

Make sure the new definitions (i.e., `CREATE TABLE` statements) are in your
database on `mysql01.service.rug.nl` as well as in
`ddwt2016/week2/assignment/bookcrud.sql`.

Step 3: Make the Application
----------------------------

An example of how the application with user accounts should function can be
seen here: https://let.webhosting.rug.nl/evang/bookcrud2

Note:

* All empty files in the starter code should be filled with code and function
  analogous to the example.
* `siegfried` has an old PHP version which does not have `password_hash()` and
  `password_verify()` yet. But they are available in `password.inc.php`, which
  you should include from your scripts using `require_once()`.
* We treat users as resources, using a similar directory structure as for
  books. For example, registering a user account is done with
  `users/create_form.php` and `users/create.php`. Updating and deleting user
  accounts is not part of this assignment.
* `books/index.php` should only display the books of the user that the user
  clicked on.
* `books/index.php` should only display the “Edit”, “Delete” and “Add book”
  links if the user whose books are displayed is currently logged in.
* `books/update.php` and `books/delete.php` must not not allow users to update
  or delete other users’ books.
* `books/create.php`, `books/update.php` and `books/delete.php` must abort with
   an error message if the user is not logged in.
* Make sure your application properly validates user input, aborts with an
  elucidating error message if invalid requests are made, prevents SQL
  injection by using prepared statements and prevents cross-site scripting by
  using `htmlspecialchars()` where appropriate.

Step 4: Submit Your Work
------------------------

*Make sure working versions of all the files you changed are in your repository
directory. If you changed them directly on the Web server, you need to copy
them back. Also, make sure your `bookcrud.sql` contains the final, error-free
version of your table definition.*

`git add` all the files you created or changed. Use `git status` to make sure
all changes are staged for commit.

`git commit` your changes with a fitting message (e.g. “assignment 2”).

Then push your changes to your private repository on Bitbucket:

    $ git push mine master

Go to https://bitbucket.org/USERNAME/ddwt2016/src (change `USERNAME` to your
Bitbucket username) to verify your commits have arrived. *Double-check that all
the changes and new files are there. If not, I will not be able to see them and
you will not receive points.* If something is missing, you may have forgotten
to add, commit or push. Use `git status` to find out what is out of sync and
repeat the necessary steps.

You do not have to submit via Nestor.

Grading
-------

The deadline is *Thursday, 8 December, 15:00*.

Your work will be graded based on the following criteria:

* satisfies the functional requirements specified here
* satisfies the non-functional requirements specified here and in the lecture
  (e.g. uses PDO with prepare/execute, does not store passwords but password
  hashes)
* code is legible (e.g. consistent indentation, meaningful variable names)
* extra points may be given for creativity (e.g. nice layout or additional
  functionality such as sorting or filtering)

*Happy hacking!*
