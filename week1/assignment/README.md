Assignment 1: Create a Simple CRUD Web Application with PHP and MySQL
=====================================================================

Step 1: Clone the course repository
-----------------------------------

Throughout this course, we will use a Git repository to which I will push all
assignments. First you need a local copy of this repository on the computer you
will do your assignments with (if you want to use multiple computers, you will
need a copy on each computer).

To get a local copy, clone the repository by running the following command on
the command line (do not type the initial dollar sign, it just a convention to
indicate the command prompt):

    $ git clone https://kevang@bitbucket.org/kevang/ddwt2016.git

This will create a new directory `ddwt2016` which contains the materials posted
so far. This directory is your copy of the repository. Move it to wherever you
would like to have it.

Step 2: Fork the course repository
----------------------------------

We will also use Git to get your assignment solutions from you to me. For this,
you will need your own private version (“fork”) of the repository on some
server.

There are a number of online services for hosting Git repositories. We will use
[Bitbucket](https://bitbucket.org/) because it offers free private
repositories.

If you do not have a Bitbucket account yet, create one.

Go to https://bitbucket.org/kevang/ddwt2016/. This is the public repository that
you already cloned.

Click *Fork* in the left menu. In the form that follows, tick the box
*This is a private repository*. Leave everything else unchanged, click
*Fork repository*. Congratulations, you now have your private repository hosted
on Bitbucket!

In order for me to see your homework, I will need read access to your private
repository. Click on the *Send invitation* button on the right and add the user
`kevang`.

The public repository is locally known as `origin`. We now need to make your
private repository locally known as `mine`. To do this, change into your
`ddwt2016` directory on the command line and run the following command, replacing
`USERNAME` with your Bitbucket username:

    $ git remote add mine https://USERNAME@bitbucket.org/USERNAME/ddwt2016.git

Step 3: Get an Overview
-----------------------

In your repository directory, go into the directory
`week1/assignment`. It contains the instructions you are reading, as well as a
`bookcrud` directory. It contains a skeleton of the Web application you will be
developing: a simple CRUD application that lets the user manage a list of
books. Some files are empty, and you will have to fill them.

A live example of such an application can be played with at
https://let.webhosting.rug.nl/evang/booknet/books. To prevent bot vandalism, it
is password-protected. You can find the credentials in the assignment on
Nestor.

(Note that Edit is another name for Update and Add is another name for Create.)

Step 4: Create a Table for Books
--------------------------------

The file `bookcrud.sql` is empty. Fill it with the definition of a MySQL table
(i.e. with a `CREATE TABLE` statement) for a table called `books`, used to
store information about books. It should contain columns for at least the
author’s name, the books’s title, a publishing date, and a unique ID.

Copy `bookcrud.sql` to your home directory the Web server
(`siegfried.webhosting.rug.nl`), e.g., using the Ubuntu File Manager or
a program like FileZilla.

Then, log into the Web server and actually crate the table in the MySQL
database. From a command line, you can do it like this:

    $ ssh siegfried.webhosting.rug.nl
    whbh02:~$ cat bookcrud.sql | mysql -h mysql01.service.rug.nl -p $USER

You will need your database credentials, which you can find in the file
`webaccount.txt` in your home directory on the Web server.

Make sure the table is created without errors.

Step 5: Create a Configuration File
-----------------------------------

In the `bookcrud` directory, copy the file `config.inc.php.sample` to
`config.inc.php`. Open `config.inc.php` and enter your database credentials
(cf. under Step 4). This will allow your Web aplication to access the database.
`config.inc.php` will not be pushed to Bitbucket and not submitted as part of
the homework; you keep it secret. Do not put your credentials into
`config.inc.php.sample`.

Step 6: Develop Your Application
--------------------------------

The `bookcrud` directory will contain your Web application. We will use a
directory structure where each type of resource has its own subdirectory.
Currently, books are our only resource type, later we will add users etc.  The
subdirectory contains handler scripts for the Create, Update and Delete
actions: `create.php`, `update.php` and `delete.php`. Additionally, it contains
scripts to display the HTML forms associated with these actions:
`create_form.php`, `update_form.php`, `delete_form.php`. Finally, there is
`index.php`, which should perform the Read action by displaying a table (or
list, or similar) of all the books currently in the database. It should also
provide links to the three forms.

All these scripts are currently empty. Your task is to fill them and make the
application work. Consult the slides and the live sample (see Nestor) for this.

Use PDO for connecting to the database (see slides). Always use
prepare/execute.

*Never put database credentials directly into your source files. Use
`require_once('../config.inc.php')` instead.* See [documentation of PHP’s
`require_once`
function](http://nl1.php.net/manual/en/function.require-once.php).

Step 7: Deploy and Test
-----------------------

Copy your Web application to the Web server. On a command line, from the
`assignment` directory, you can easily do it like this:

    $ rsync -drvz bookcrud siegfried.webhosting.rug.nl:~/site/bookcrud

You can then test it at
http://siegfried.webhosting.rug.nl/~YOURUSERNAME/bookcrud.

Keep developing and copying the app to the Web server (just repeat the above
command) until it works. Optionally, you can commit changes in-between with
Git.

Step 8: Submit Your Work
------------------------

*Make sure working versions of all the files you changed are in your repository
directory. If you changed them directly on the Web server, you need to copy
them back. Also, make sure your `bookcrud.sql` contains the final, error-free
version of your table definition.*

`git add` all the files you created or changed. Use `git status` to make sure
all changes are staged for commit.

`git commit` your changes with a fitting message (e.g. “assignment 1”).

Then push your changes to your private repository on Bitbucket:

    $ git push mine master

Go to https://bitbucket.org/USERNAME/ddwt2016/src (change `USERNAME` to your
Bitbucket username) to verify your commits have arrived. *Double-check that all
the changes and new files are there. If not, I will not be able to see them and
you will not receive points.* If something is missing, you may have forgotten
to add, commit or push. Use `git status` to find out what is out of sync and
repeat the necessary steps.

You do not have to submit via Nestor.

Grading
-------

The deadline is *Thursday, November 24, 15:00*.

Your work will be graded based on the following criteria:

* satisfies the functional requirements specified here (e.g. books can be
  created, read, updated and deleted)
* satisfies the non-functional requirements specified here (e.g. uses PDO with
  prepare/execute)
* code is legible (e.g. consistent indentation, meaningful variable names)
* extra points may be given for creativity (e.g. nice layout or additional
  functionality such as sorting or filtering)

*Happy hacking!*
